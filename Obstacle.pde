class Obstacle {
    static final int radialAtractionDist = 20;
    PVector position;
    boolean isAttraction;
    color c;
    float radius;
    Obstacle(PVector position, boolean isAttraction, float radius) {
        this.position = position;
        this.isAttraction = isAttraction;
        this.radius = radius;
        if(isAttraction) {
            c = red;
        } else {
            c = green;
        }
    }

    void update(ArrayList<Boid> boids) {
        for (int i = 0; i < boids.size(); i++) {
          Boid currBoid = boids.get(i);
          float d = this.position.dist(currBoid.position);
          if (d < radialAtractionDist) {
            if(isAttraction) {
                currBoid.f_obstacle.add(PVector.sub(this.position, currBoid.position));
            } else {
                currBoid.f_obstacle.add(PVector.sub(currBoid.position, this.position));
            }
          }
      }
    }

    void draw() {
      fill(c);
      drawCircle(this.position,this.radius);
  }
}