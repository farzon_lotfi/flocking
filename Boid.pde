PVector V(float x, float y) {
    return new PVector(x,y);
}
PVector V() {
    return new PVector(0,0);
}

void drawCircle(PVector C, float r) {
  ellipse(C.x, C.y, 2*r, 2*r);
}

float checkBounds(float pos, float boundary) {
    if (pos > boundary) {
        return 0;
    } else if (pos < 0) {
        return boundary;
    } else {
        return pos;
    }
}

class Boid {
  int index;
  PVector position;
  PVector velocity;
  PVector f_c, f_ca, f_vm, f_w, f_attract, f_obstacle;
  PVector acceleration; //v'
  float radius;
  float mass;
  static final float maxForce = .8;
  static final float maxSpeed = 4;
  static final float minSpeed = .1;
  static final int radialAtractionDist = 120;
  static final int radialDistCA = 20;
  static final int radialDistVM = 60;
  static final int radialDistFC = 60;
  static final float CAWeight = 0.9; 
  static final float VMWeight = 0.8;
  static final float FCWeight = 0.03;

  Boid(int index, PVector pos, float radius, float mass) {
      this.index = index;
      this.position = pos;
      this.radius = radius;
      this.mass = mass;
      f_c= V(); f_ca= V(); f_vm= V(); f_w= V();
      f_attract = V();
      f_obstacle = V();
      this.velocity = V();
      this.acceleration = V();
  }

  void teleport() {
      this.position.x = random(radius, width-radius);
      this.position.y = random(radius, height-radius);
  }

  void wrap() {
    this.position.x = checkBounds(this.position.x, width);
    this.position.y = checkBounds(this.position.y, height);
  }

  void wallBounce() {
    float wallBounceMag = -60; // negative to reverse direction on a multipy equal
    float directionStepSizeWeight = .25;
    if (this.position.x <= 0 || this.position.x >= width-radius) {
        this.velocity.x *=  wallBounceMag;
    }
    if (this.position.y <= 0 || this.position.y >= height-radius) {
       this.velocity.y *= wallBounceMag;
    }
    if (this.position.x>=width||this.position.x+radius>=width){
        this.position.x = width - (radius+1);
    }
    if (this.position.y >= height || this.position.y + radius >= height){
      this.position.y = height - (radius+1);
    }
    if (this.position.y<=0){
      this.position.y = radius;
      this.position.y += radius*directionStepSizeWeight;
    }
    if (this.position.y>=height){
        this.position.y = (height-radius);
        this.position.y -= radius*directionStepSizeWeight;
    }
    if (this.position.x<=0){
      this.position.x =radius;
      this.position.x += radius*directionStepSizeWeight;
    }
    if (this.position.x>=width){
        this.position.y = (width-radius);
        this.position.x -= radius*directionStepSizeWeight;
    }
  }

  void computeAcceleration() {
    this.acceleration.add(f_vm);
    this.acceleration.add(f_c);
    this.acceleration.add(f_ca);
    this.acceleration.add(f_w);
    this.acceleration.add(f_attract);
    this.acceleration.add(f_obstacle);
  }

  float computeWij(float dij) {
      float e = .001;
      return 1.0f/(sq(dij)+e);
  }

  void computeFlockingCenterForce(PVector currBoidPos, float weightij) {
      if(!flockCentering) {
        return;
      }
      
      //f_c += wij * (currBoidPos-this.position)
      f_c.add(PVector.sub(currBoidPos, this.position).mult(weightij));
  }

  void computeCollisionAvoidanceForce(PVector currBoidPos, float weightij) {
      if(!collisionAvoidance) {
          return;
      }
      //f_ca += wij * (this.pos -currBoidPos)
      f_ca.add(PVector.sub(this.position, currBoidPos).mult(weightij));
  }

  void velocityMatching(PVector currBoidVel, float weightij) {
      if(!velocityMatching) {
          return;
      }
      
      //currBoidVel - this.v
      f_vm.add(PVector.sub(currBoidVel, this.velocity).mult(weightij));
  }

  void wanderingForce(){
    if(!wanderingForce) {
        return;
    }
    f_w = new PVector(noise(this.position.x)*random(-1,1), noise(this.position.y)*random(-1,1));
    f_w.normalize();
    f_w.setMag(maxSpeed);
    f_w.limit(maxForce);
  }
  void forceAccumulator(ArrayList<Boid> boids) {
      float weightijSumCA = 0.0;
      float weightijSumFC = 0.0;
      float weightijSumVM = 0.0;
      for (int i = 0; i < boids.size(); i++) {
          Boid currBoid = boids.get(i);
          if(currBoid == this) {
              continue;
          }
          float d = this.position.dist(currBoid.position);
          float weightij = computeWij(d);
          if (d < radialDistCA) {
              computeCollisionAvoidanceForce(currBoid.position, weightij);
              weightijSumCA += weightij;
          }
          if (d < radialDistVM) {
              velocityMatching(currBoid.velocity,weightij);
              weightijSumVM += weightij;
          }
          if (d < radialDistFC) {
              computeFlockingCenterForce(currBoid.position,weightij);
              weightijSumFC += weightij;
          }
          wanderingForce();
          attractionForce();
      }
      if(weightijSumFC > 0) {
          f_c.div(weightijSumFC);
          f_c.mult(FCWeight);
          f_c.limit(maxForce);

      }
      if(weightijSumCA > 0) {
          f_ca.div(weightijSumCA);
          f_ca.mult(CAWeight);
          f_ca.limit(maxForce);
      }
      if(weightijSumVM > 0) {
          f_vm.div(weightijSumVM);
          f_vm.mult(VMWeight);
          f_vm.limit(maxForce);
      }
  }

   void normalizeForceHelper(PVector f) {
      f.setMag(maxSpeed);
      f.sub(this.velocity);
      f.limit(maxForce);
   }
  void normalizeForce(PVector f,float total) {
      f.div(total);
      normalizeForceHelper(f);
      
  }

  void attractionForce(){
    if(mousePressed) {
        if  ( !battractionForceRange|| (this.position.dist(mousePos) < radialAtractionDist)) {
            if(attractionMode) {
                f_attract = PVector.sub(mousePos, this.position);
            }
            if(repulsionMode) {
                f_attract = PVector.sub(this.position, mousePos);
            }
        }
    }
  }

  void aggregateForces(ArrayList<Boid> boids) {
      f_vm  = new PVector();
      f_c = new PVector();
      f_ca = new PVector();
      f_w = new PVector();
      f_attract = V();
      forceAccumulator(boids);
      computeAcceleration();
      
  }

  void update() {
    this.velocity.add(this.acceleration);
    this.velocity.limit(maxSpeed);
    this.position.add(this.velocity);
    this.acceleration = V();
  }
  void draw() {
      fill(blue);
      //drawCircle(this.position,this.radius);
      pushMatrix();
      float theta = this.velocity.heading() + radians(90);
      translate(this.position.x,this.position.y);
      rotate(theta);
      beginShape();
      vertex(0, -this.radius);
      vertex(-this.radius, this.radius);
      vertex(this.radius, this.radius);
      endShape(CLOSE);
      popMatrix();
  }
}
