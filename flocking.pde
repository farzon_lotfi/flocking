
color black=#000000, white=#FFFFFF, // set more colors using Menu >  Tools > Color Selector
   red=#FF0000, green=#00FF01, blue=#0300FF, yellow=#FEFF00, cyan=#00FDFF, magenta=#FF00FB,
   grey=#818181, orange=#FFA600, brown=#B46005, metal=#B5CCDE, 
   lime=#A4FA83, pink=#FCC4FA, dgreen=#057103,
   lightWood=#F5DEA6, darkWood=#D8BE7A;
   
Flock flock;
Boolean attractionMode = false, repulsionMode = false, bPaused = false;
Boolean flockCentering = true, velocityMatching = true, battractionForceRange=false;
Boolean collisionAvoidance = true, wanderingForce = true, obstacleMode= false;
Boolean drawTrail  = false, wrapAround  = true;
PVector mousePos = V();
void setup() {
    size(800, 600);
    background(lime);
    flock = new Flock(16);
}
void writeLine(String S, int i) {
  // writes S at line i
  text(S, 30, 25+i*20);
}
void mouseClicked() {
  if(obstacleMode && (attractionMode || repulsionMode)) {
        flock.addObstacle(attractionMode,mousePos);
  }
}

void draw() {
    if(!drawTrail) {
      background(lime);
    }

    if(mousePressed){
      mousePos = V(mouseX, mouseY);
      
    }
    if(obstacleMode) {
      flock.drawObstacles();
    }
    flock.drawBoids();
    fill(magenta);
    int L=0; // line counter, incremented below for ech line
    writeLine("(press a) attraction: " + attractionMode.toString(),L++);
    writeLine("(press r) repulsion: " + repulsionMode.toString(), L++);
    writeLine("(press s) to scatter.", L++);
    writeLine("(press p) leave path: " + drawTrail.toString(), L++);
    writeLine("(press c) to clear path and obstacles.", L++);
    writeLine("Boid count: " + flock.size() , L++);
    writeLine("press '+' to add boids '-' to remove them", L++);
    writeLine("(press 1) flock centering: " + flockCentering.toString() , L++);
    writeLine("(press 2) velocity matching: " + velocityMatching.toString() , L++);
    writeLine("(press 3) collision avoidance: " + collisionAvoidance.toString() , L++);
    writeLine("(press 4) wandering force: " + wanderingForce.toString() , L++);
    writeLine("(press y) attraction force Range on: " + battractionForceRange.toString() , L++);
    writeLine("(press o) obstacle mode : " + obstacleMode.toString(),L++);
    writeLine("(press w) wrap around mode : " + wrapAround.toString(),L++);
    writeLine("(press spacebar) Game is: " + (bPaused ? "paused" : "running") , L++);
}

void keyPressed() {
  switch(key) {

  // a - Switch to attraction mode (for continuous attraction when mouse is held down).
  case 'a':
  case 'A':
  attractionMode = !attractionMode;
  repulsionMode = false;
  break;

  // r - Switch to repulsion mode (for continuous repulsion when mouse is held down).
  case 'r':
  case 'R':
  repulsionMode = !repulsionMode;
  attractionMode = false;
  break;

  //s - Cause all creatures to be instantly scattered to random positions in the window, and with random directions.
  case 's':
  case 'S':
  flock.scatter();
  break;

  // p - Toggle whether to have creatures leave a path, that is, whether the window is cleared each display step or not.
  case 'p':
  case 'P':
  drawTrail = !drawTrail;
  break;

  // c - Clear the screen, but do not delete any boids. (This can be useful when creatures are leaving paths.)
  case 'c':
  case 'C':
  flock.clearObstacles();
  background(lime);
  break;

  // 1 - Toggle the flock centering forces on/off.
  case '1':
  flockCentering = !flockCentering;
  break;

  // 2 - Toggle the velocity matching forces on/off.
  case  '2':
  velocityMatching = !velocityMatching;
  break; 

  // 3 - Toggle the collision avoidance forces on/off.
  case '3':
  collisionAvoidance = ! collisionAvoidance;
  break;

  // 4 - Toggle the wandering force on/off.
  case '4':
  wanderingForce = !wanderingForce;
  break;

   //=,+ - Add one new creature to the simulation. You should allow up to 100 creatures to be created.
   case '+':
   case '=':
   if(flock.size() < 100) {
    flock.addBoid();
   }
   break;

  //- (minus sign) - Remove one new creature from the simulation (unless there are none already).
  case '-':
  flock.removeBoid();
  break;

  // space bar - Start or stop the simulation (toggle between these).
  case ' ':
  bPaused = !bPaused;
  break;
  
  case 'y':
  battractionForceRange = !battractionForceRange;
  break;
  case 'o':
  obstacleMode = !obstacleMode;
  break;
  case 'w':
    wrapAround = !wrapAround;
    break;
  }
}
