class Flock {
    ArrayList<Boid> boids;
    ArrayList<Obstacle> obstacles;
    float boidMass = 1.0f;
    float boidRadius = 5.0f;
    float obstacleRadius = 10.0f;
    Flock(int n) {
        boids = new ArrayList<Boid>();
        obstacles = new ArrayList<Obstacle>();
        createBoids(n);
    }

    void scatter() {
        for (int i=0; i < boids.size(); i++) {
            Boid currBoid = boids.get(i);
            currBoid.teleport();
            currBoid.velocity = V();
        }
    }
    void drawBoids() {
        for (int i=0; i < boids.size(); i++) {
            Boid currBoid = boids.get(i);
            if(!bPaused) {
                if(wrapAround) { 
                    currBoid.wrap();
                }
                currBoid.aggregateForces(boids);
                currBoid.update();
                if(!wrapAround) {
                    currBoid.wallBounce();
                }
            }
            currBoid.draw();
        }
    }

    void drawObstacles() {
        for (int i = 0; i < boids.size(); i++) {
            Boid currBoid = boids.get(i);
            currBoid.f_obstacle = V();
        }
        for (int i=0; i < obstacles.size(); i++) {
            Obstacle currObstacle = obstacles.get(i);
            currObstacle.draw();
            currObstacle.update(boids);
        }
    }

    void addBoid() {
        boids.add(new Boid(boids.size(), V(random(width), random(height)),boidRadius,boidMass));
    }

    void addObstacle(boolean isAttractionForce, PVector position) {
        for(int i = 0; i < obstacles.size();i++) {
            Obstacle currObstacle = obstacles.get(i);
            float dx = position.x - currObstacle.position.x;
            float dy = position.y - currObstacle.position.y;
            float distance = sqrt(dx * dx + dy * dy);
            if (distance < obstacleRadius + obstacleRadius) {
                // collision detected!
                obstacles.remove(i);
                return;
            }
        }
        obstacles.add(new Obstacle(position, isAttractionForce, obstacleRadius));
    }
    void clearObstacles() {
        obstacles = new ArrayList<Obstacle>();
    }
    
    boolean removeBoid() {
        int size = boids.size();
        if(size> 0) {
            boids.remove(size - 1);
            return true;
        }
        return false;
    }
    int size() {
        return boids.size();
    }
    void createBoids(int n) {
       for (int i = 0; i < n; i++) {
         Boid boid = new Boid(i, V(random(width), random(height)),boidRadius,boidMass);
         boids.add(boid);
       }
    }
}
